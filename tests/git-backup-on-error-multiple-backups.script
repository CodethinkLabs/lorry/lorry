#!/bin/bash
# SPDX-License-Identifier: GPL-2.0-only

# Tests when a git mirror fails that it keeps the backups around
#
# Copyright (C) 2012-2013  Codethink Limited

set -e
set -o pipefail


logfile="$DATADIR/git-backup-test-repo.log"
workdir="$DATADIR/work-dir"
repo="$DATADIR/git-backup-test-repo"

normalize() {
    DATETIMESPEC='[0-9]*-[0-9]*-[0-9]*-[0-9]*:[0-9]*:[0-9]*'
    sed -r -e "s|git-pre-update-$DATETIMESPEC|git-pre-update-DATETIME|g" \
           -e "s|git-post-fail-$DATETIMESPEC|git-post-fail-DATETIME|g" \
           -e "s/pack-[0-9a-z]+\.(idx|pack)$/pack-file/" \
           -e '/hooks\/.*\.sample/d' \
           -e "/\/objects\/info\/commit-graph$/d" \
           -e "/\/objects\/pack\/pack-[0-9a-z]+\.bitmap$/d" \
           -e "s|$DATADIR|DATADIR|g" "$@"
}

# mirror some history
"${SRCDIR}/test-lorry" --pull-only --log="$logfile" --working-area="$workdir" --bundle=never \
  "$DATADIR/git-backup-test-repo.lorry" | normalize 

# make upstream disappear to cause errors
rm -rf "$repo"
if "${SRCDIR}/test-lorry" --pull-only --log="$logfile" --working-area="$workdir" \
           "$DATADIR/git-backup-test-repo.lorry" --bundle=never 2>/dev/null \
           --keep-multiple-backups | normalize
then
    echo Previous lorry command should have failed >&2
    exit 1
else
    find "$workdir/git-backup-test-repo" | LC_ALL=C sort | normalize
    grep -H . "$workdir/git-backup-test-repo/git"*"/lorry-update-count" | normalize
fi
