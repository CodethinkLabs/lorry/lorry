#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-only

# Tests that all refs are pushed if no push-refspecs are given
#
# Copyright (C) 2012  Codethink Limited

set -e

git_upstream="$DATADIR"/git-upstream
logfile="$DATADIR"/no-pushspec-pushall.log
workdir="$DATADIR"/work-dir

lorryfile="$DATADIR"/no-pushspec.lorry
cat >"$lorryfile" <<EOF
{
    "no-pushspec": {
        "type": "git",
        "url": "file://$git_upstream"
    }
}
EOF

mirror_path="$DATADIR"/git-mirror
mkdir -p "$mirror_path"
git init --quiet --bare "$mirror_path"/no-pushspec.git

"${SRCDIR}/test-lorry" --log="$logfile" --working-area="$workdir" \
        --mirror-base-url-push=file://"$mirror_path" \
        --mirror-base-url-fetch=file://"$mirror_path" \
        "$lorryfile"

# verify that the git repository has all the refs that upstream does
test "$(cd "$mirror_path"/no-pushspec.git && git for-each-ref | sort)" = \
     "$(cd "$git_upstream" && git for-each-ref | sort)"
