#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-only

# Creates a bzr repository with a single file and a single commit.
#
# Copyright (C) 2011-2012, 2020  Codethink Limited

set -e

# If bzr is not available and brz is, use brz instead
if ! command -v bzr >/dev/null && command -v brz >/dev/null; then
    bzr() { brz "$@"; }
fi

# create the repository
repo="$DATADIR/bzr-test-repo"
mkdir "$repo"
cd "$repo"
bzr init --quiet trunk

# add the test file
cd "$repo/trunk"
echo "first line" > test.txt
bzr add --quiet test.txt

# make a commit
bzr commit --quiet -m "first commit"

# create the .lorry file for the repository
cat <<EOF > $DATADIR/bzr-test-repo.lorry
{
  "bzr-test-repo": {
    "type": "bzr",
    "url": "file://$repo/trunk"
  }
}
EOF


# create the working directory
test -d "$DATADIR/work-dir" || mkdir "$DATADIR/work-dir"
