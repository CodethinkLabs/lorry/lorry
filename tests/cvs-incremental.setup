#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-only

# Creates a CVS repository with a single file and a single commit.
#
# Copyright (C) 2012, 2020  Codethink Limited

set -e

# CVS wants $USER, $LOGNAME, and $LOGNAME set to a real username.
export USER=root
export LOGNAME=$USER
export USERNAME=$USER

# create the repository
repo="$DATADIR/cvs-test-repo"
export CVSROOT="$repo"
export CVS_RSH=
cvs init

# create a local working copy
workingcopy="$DATADIR/cvs-test-checkout"
mkdir "$workingcopy"
cd "$workingcopy"
cvs checkout CVSROOT
cd "$workingcopy/CVSROOT"

# ensure that our commit has a later timestamp than cvs init's
# "initial checkin"
sleep 1

# add the test file
echo "first line" > test.txt
cvs -Q add test.txt

# make a commit
cvs -Q commit -m "first commit"

# create the .lorry file for the repository
cat <<EOF > $DATADIR/cvs-test-repo.lorry
{
  "cvs-test-repo": {
    "type": "cvs",
    "url": "$repo",
    "module": "CVSROOT"
  }
}
EOF


# create the working directory
test -d "$DATADIR/work-dir" || mkdir "$DATADIR/work-dir"
