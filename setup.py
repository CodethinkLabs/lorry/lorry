# SPDX-License-Identifier: GPL-2.0-only

# Copyright (C) 2012-2015, 2019 Codethink Limited

"""Setup.py for lorry."""

import sys

from setuptools import setup

if sys.version_info < (3, 6):
    print("lorry requires Python 3.6+", file=sys.stderr)
    sys.exit(1)

setup()
