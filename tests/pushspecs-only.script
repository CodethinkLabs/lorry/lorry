#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-only

# Tests that only the specified refs are pushed
#
# Copyright (C) 2012  Codethink Limited

set -e

git_upstream="$DATADIR"/git-upstream
logfile="$DATADIR"/pushspecs.log
workdir="$DATADIR"/work-dir

lorryfile="$DATADIR"/pushspecs.lorry
cat >"$lorryfile" <<EOF
{
    "pushspecs": {
        "type": "git",
        "url": "file://$git_upstream",
        "refspecs": [
            "master",
            "refs/tags/rc/*"
        ]
    }
}
EOF

mirror_path="$DATADIR"/git-mirror
mkdir -p "$mirror_path"
git init --quiet --bare "$mirror_path"/pushspecs.git

"${SRCDIR}/test-lorry" --log="$logfile" --working-area="$workdir" \
        --mirror-base-url-push=file://"$mirror_path" \
        --mirror-base-url-fetch=file://"$mirror_path" \
        "$lorryfile"

allrefs="$(cd "$mirror_path"/pushspecs.git && git for-each-ref | sort)"
wantedrefs="$(cd "$mirror_path"/pushspecs.git &&
              git for-each-ref refs/heads/master 'refs/tags/rc/*' | sort)"
test "$allrefs" = "$wantedrefs"
